<?php
$config = array(
    'admin' => array(
        'core:AdminPassword',
    ),

    'default-sp' => array(
        'exampleauth:UserPass',
        'student:studentpass' => array(
            'eduPersonTargetedID' => array('student'),
            'mail' => array('student@localhost.localdomain'),
            'cn' => array('Student'),
        ),
        'admin:adminpass' => array(
            'eduPersonTargetedID' => array('admin'),
            'mail' => array('admin@localhost.localdomain'),
            'cn' => array('Admin'),
        )
    )
);
