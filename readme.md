# FileSender with with DTN Agent Docker Install #

These instructions walk you through the process of installing FileSender 2.9 with SimpleSAMLphp 1.17.6, MariaDB, GridFTP server, and DTN Agent 0.1.1 as Docker containers.

## Requirements ##

* Docker
* git
* wget
* unzip
* openssl
* patch
* pwgen

Also, the system's hostname must resolve to a valid IP.

**Optional:** Host networking will be used by the containers. You may want to disable the Docker bridge and prevent Docker from making changes to your firewall rules by editing the Docker `daemon.json` file on your system.

~~~~
{
  "bridge": "none",
  "iptables": false
}
~~~~

## Environment ##
The following environmental variables are used by the following scripts.

Set the password for the *filesender* SQL account.
~~~~
export SQL_FS_PASS=`pwgen -s 20`
~~~~

Set the directory where the dtn_agent_example respository will be downloaded.
~~~~
export DTNA_EX=~/build/dtn_agent_example
~~~~

These other direcorties will be set based on `DTNA_EX`.
~~~~
export FS_BUILD_DIR=$DTNA_EX/filesender
export DTNA_BUILD_DIR=$DTNA_EX/dtn_agent
export SAML_SRC=$FS_BUILD_DIR/simplesamlphp-1.17.6
export FS_SRC=$FS_BUILD_DIR/filesender-filesender-2.9
~~~~

Get the primary IP address of the system. This may not work on some systems, check to make sure the variable is correct.
~~~~
export PRIMARY_IP=`ip route get 8.8.8.8 | awk '{print $7; exit}'`
echo $PRIMARY_IP
~~~~

**Optional:** If a dedicated partition is going to be used for file storage set the path to the device and the file system type. The partion needs to be formatted in advanced.
~~~~
export DATA_PART_TYPE=xfs
export DATA_PART_DEVICE=/dev/sd??
~~~~

Download the DTN Agent Example Repository
~~~~
mkdir -p $DTNA_EX
git clone https://bitbucket.org/uva-sne/dtn_agent_example.git $DTNA_EX
~~~~

## MariaDB Container ##
Download and start the MariaDB contianer.
~~~~
docker run -d --network host --name filesender-db \
 -e MYSQL_ROOT_PASSWORD=`pwgen -s 20` mariadb:latest --bind-address 127.0.0.1
~~~~

Configure the FileSender database. The MariaDB server will not accept connections until it has finished initilizing. If you receive an access denied message check that it has started listening on TCP port 3306.
~~~~
docker exec -it filesender-db bash -c "mysql -u root --password=\"\$MYSQL_ROOT_PASSWORD\" <<'EOF'
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.db WHERE Db='test' OR Db='test_%';

CREATE DATABASE IF NOT EXISTS \`filesender\` DEFAULT CHARACTER SET utf8mb4;
GRANT USAGE ON *.* TO 'filesender'@'127.0.0.1' IDENTIFIED BY '$SQL_FS_PASS';
GRANT CREATE, CREATE VIEW, ALTER, SELECT, INSERT, INDEX, UPDATE, DELETE ON \`filesender\`.* TO 'filesender'@'127.0.0.1';
GRANT DROP ON \`filesender\`.* TO 'filesender'@'127.0.0.1';
GRANT ALL ON \`filesender\`.* TO 'filesender'@'127.0.0.1';
FLUSH PRIVILEGES;
exit
EOF
"
~~~~

## FileSender Source ##

Download the FileSender source code.
~~~~
cd $FS_BUILD_DIR
wget https://github.com/filesender/filesender/archive/filesender-2.9.zip
unzip filesender-2.9.zip
rm -f filesender-2.9.zip
~~~~

Create the FileSender configuration file. Note that this also configures the connection between FileSender and the DTN Agent.
~~~~
cd $FS_BUILD_DIR
envsubst '$SQL_FS_PASS $HOSTNAME $PRIMARY_IP' < filesender-config.php.tmpl > $FS_SRC/config/config.php
~~~~

Patch the FileSender code to support the DTN Agent.
~~~~
cd $FS_BUILD_DIR
git clone https://bitbucket.org/uva-sne/filesender_dtn.git
cd filesender_dtn
git checkout 9cf7a64
cp $FS_BUILD_DIR/filesender_dtn/dtn_check.php $FS_SRC/classes/data/
patch $FS_SRC/classes/data/Transfer.class.php $FS_BUILD_DIR/filesender_dtn/dtn.patch
~~~~

### SimpleSAMLphp Source ###

Download the SimpleSAMLphp soucre code.
~~~~
cd $FS_BUILD_DIR
wget https://github.com/simplesamlphp/simplesamlphp/releases/download/v1.17.6/simplesamlphp-1.17.6.tar.gz
tar -xf simplesamlphp-1.17.6.tar.gz
rm -f simplesamlphp-1.17.6.tar.gz
~~~~

Configure SimpleSAMLphp to use the *exampleauth* method and create some test accounts.
~~~~
\cp -r $SAML_SRC/config-templates/*.php $SAML_SRC/config/
\cp -r $SAML_SRC/metadata-templates/*.php $SAML_SRC/metadata/
\cp $FS_BUILD_DIR/saml-authsources.php $SAML_SRC/config/authsources.php

sed -i "s/'auth.adminpassword' => '123',/'auth.adminpassword' => 'adminpass',/g" $SAML_SRC/config/config.php
sed -i "s/'secretsalt' => 'defaultsecretsalt',/'secretsalt' => '`pwgen -s 20`',/g" $SAML_SRC/config/config.php
sed -i "s/'enable.saml20-idp' => false/'enable.saml20-idp' => true/g" $SAML_SRC/config/config.php

touch $SAML_SRC/modules/exampleauth/enable
~~~~

Generate the certificate needed for SimpleSAMLphp
~~~~
openssl req -newkey rsa:2048 -new -x509 -days 3652 -nodes -out $SAML_SRC/cert/server.crt -keyout $SAML_SRC/cert/server.pem -subj "/CN=$HOSTNAME"
~~~~

## FileSender Container ##

**Optional:** If a dedicated partition is going to be used for file storage create the docker volume.
~~~~
docker volume create --driver local --opt type=$DATA_PART_TYPE --opt device=$DATA_PART_DEVICE filesender-vol
~~~~

Build and start the FileSender container.
~~~~
cd $FS_BUILD_DIR
docker build --network=host -t filesender .

docker run -v filesender-vol:/opt/filesender/filesender/files \
  -d --network host --name filesender filesender
~~~~

Execute the database update script in the FileSender container, Then change the ownership of the log file to the Apache user.
~~~~
docker exec -it filesender php /opt/filesender/filesender/scripts/upgrade/database.php

docker exec -it filesender chown -R www-data:www-data /opt/filesender/filesender/log
~~~~

## DTN Agent Container ##

Download the DTN Agent source code.
~~~~
cd $DTNA_BUILD_DIR
git clone https://bitbucket.org/uva-sne/dtn_agent.git
cd $DTNA_BUILD_DIR/dtn_agent
git checkout e3c0c88
~~~~

Generate the certificates needed for secure WebSocket connections.
~~~~
mkdir -p $DTNA_BUILD_DIR/dtn_agent/pki
cd $DTNA_BUILD_DIR/dtn_agent/pki
openssl req -newkey rsa:2048 -new -x509 -days 3652 -nodes -out cert.pem -keyout key.pem -subj "/CN=$HOSTNAME"
~~~~

Generate the configuration file for the DTN-Agent
~~~~
envsubst '$PRIMARY_IP' < $DTNA_EX/dtn_agent/config.json.tmpl > $DTNA_EX/dtn_agent/dtn_agent/config/$HOSTNAME.json
~~~~

Build and run the DTN Agent container.
~~~~
cd $DTNA_BUILD_DIR
docker build --network host -t dtn_agent .

docker run --network host -v filesender-vol:/fastdata/data: -w /usr/src/dtn_agent \
  -d --restart always --name dtn_agent dtn_agent node dtn_agent
~~~~

## GridFTP Container ##

Generate the GridFTP Server configuration file.
~~~~
envsubst '$PRIMARY_IP' < $DTNA_EX/GridFTP/gridftp-server.conf.tmpl > $DTNA_EX/GridFTP/gridftp-server.conf
~~~~

Build and run the DTN Agent container.
~~~~
cd $DTNA_EX/GridFTP
docker build --network host -t gridftp .

docker run --network host -v filesender-vol:/fastdata/data: \
  -d --restart always --name gridftp gridftp
~~~~


